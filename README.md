# PICA-Daten zu den halleschen Zeitungen und Zeitschriften

## Datenquellen

### Zeitschriftendatenbank ([DE-600](https://sigel.staatsbibliothek-berlin.de/suche/?isil=DE-600))

> Die Zeitschriftendatenbank (ZDB) ist eine der weltweit größten Datenbanken für den Nachweis von Zeitschriften und Zeitungen aus allen Ländern, in allen Sprachen, ohne zeitliche Einschränkung, in gedruckter, elektronischer oder anderer Form. ... Aktuell bringen 3.700 Bibliotheken aus Deutschland und Österreich ihre Zeitschriftentitel und die zugehörigen Bestandsnachweise in die ZDB ein. ... Ingesamt umfasst die ZDB mehr als 1,8 Mio. Titel- und 16,6 Mio. Bestandsnachweise. ([Quelle](https://sigel.staatsbibliothek-berlin.de/suche/?isil=DE-600))

_Search and Retrieve via URL_

- Dokumentation: https://www.zeitschriftendatenbank.de/services/schnittstellen/sru/
- Endpunkt: http://services.dnb.de/sru/zdb
- Format: [PicaPlus-xml](http://format.gbv.de/pica/ppxml)
- Abfrage: [dnb.eje>=1688 and dnb.eje<=1815 and vort any "Halae Halis Halle Halle,S. Halle,Saale"](http://services.dnb.de/sru/zdb?version=1.1&operation=searchRetrieve&query=dnb.eje%3E%3D1688%20and%20dnb.eje%3C%3D1815%20and%20vort%20any%20%22Halae%20Halis%20Halle%20Halle%2CS.%20Halle%2CSaale%22&recordSchema=PicaPlus-xml&maximumRecords=5)
- Datenlizenz: [CC0 1.0](https://www.zeitschriftendatenbank.de/de/ueber-uns/datenlizenz/)

### Gemeinsamer Verbundkatalog ([DE-627](https://sigel.staatsbibliothek-berlin.de/suche/?isil=DE-627))

> Der gemeinsame Verbundkatalog (GVK) ist der frei zugängliche Ausschnitt der Verbunddatenbank K10plus mit den Nachweisen der GBV-Bibliotheken und der SWB-Bibliotheken. ... Im gemeinsamen Verbundkatalog (GVK) sind über 65 Mio. Titel von Büchern, Zeitschriften, Aufsätzen, Kongressberichten, Mikroformen, elektronischen Dokumenten, Datenträgern, Musikalien, Karten etc. von über 1.000 Bibliotheken aus dem GBV und dem SWB nachgewiesen. ([Quelle](https://kxp.k10plus.de/DB=2.1/))

_Search and Retrieve via URL_

- Dokumentation: https://wiki.k10plus.de/display/K10PLUS/SRU
- Endpunkt: http://sru.k10plus.de/gvk
- Format: [picaxml](http://format.gbv.de/pica/xml)
- Abfragen: [pica.ver=Halae](http://sru.k10plus.de/gvk?version=2.0&operation=searchRetrieve&query=pica.ver%3DHalae&recordSchema=picaxml&maximumRecords=5), [pica.ver=Halis](http://sru.k10plus.de/gvk?version=2.0&operation=searchRetrieve&query=pica.ver%3DHalis&recordSchema=picaxml&maximumRecords=5), [pica.ver=Halle](http://sru.k10plus.de/gvk?version=2.0&operation=searchRetrieve&query=pica.ver%3DHalle&recordSchema=picaxml&maximumRecords=5)
    - das Abrufen der fast 300.000 Datensätze zur letztgenannten Abfrage scheitert, entsprechend wurden die Daten anhand ihrer jeweiligen bibliographischen Gattungsangabe in Untermengen geteilt und bereitgestellt:
        - [pica.ver=Halle and pica.bbg=Aar](http://sru.k10plus.de/gvk?version=2.0&operation=searchRetrieve&query=pica.ver%3DHalle%20and%20pica.bbg%3DAar&recordSchema=picaxml&maximumRecords=5), [pica.ver=Halle and pica.bbg=Aau](http://sru.k10plus.de/gvk?version=2.0&operation=searchRetrieve&query=pica.ver%3DHalle%20and%20pica.bbg%3DAau&recordSchema=picaxml&maximumRecords=5), [pica.ver=Halle and pica.bbg=Aaus](http://sru.k10plus.de/gvk?version=2.0&operation=searchRetrieve&query=pica.ver%3DHalle%20and%20pica.bbg%3DAaus&recordSchema=picaxml&maximumRecords=5), [pica.ver=Halle and pica.bbg=Aav](http://sru.k10plus.de/gvk?version=2.0&operation=searchRetrieve&query=pica.ver%3DHalle%20and%20pica.bbg%3DAav&recordSchema=picaxml&maximumRecords=5), [pica.ver=Halle and pica.bbg=Aavs](http://sru.k10plus.de/gvk?version=2.0&operation=searchRetrieve&query=pica.ver%3DHalle%20and%20pica.bbg%3DAavs&recordSchema=picaxml&maximumRecords=5), [pica.ver=Halle and pica.bbg=Abu](http://sru.k10plus.de/gvk?version=2.0&operation=searchRetrieve&query=pica.ver%3DHalle%20and%20pica.bbg%3DAbu&recordSchema=picaxml&maximumRecords=5), [pica.ver=Halle and pica.bbg=Abv](http://sru.k10plus.de/gvk?version=2.0&operation=searchRetrieve&query=pica.ver%3DHalle%20and%20pica.bbg%3DAbv&recordSchema=picaxml&maximumRecords=5), [pica.ver=Halle and pica.bbg=Acr](http://sru.k10plus.de/gvk?version=2.0&operation=searchRetrieve&query=pica.ver%3DHalle%20and%20pica.bbg%3DAcr&recordSchema=picaxml&maximumRecords=5), [pica.ver=Halle and pica.bbg=Acu](http://sru.k10plus.de/gvk?version=2.0&operation=searchRetrieve&query=pica.ver%3DHalle%20and%20pica.bbg%3DAcu&recordSchema=picaxml&maximumRecords=5), [pica.ver=Halle and pica.bbg=Acus](http://sru.k10plus.de/gvk?version=2.0&operation=searchRetrieve&query=pica.ver%3DHalle%20and%20pica.bbg%3DAcus&recordSchema=picaxml&maximumRecords=5), [pica.ver=Halle and pica.bbg=Acv](http://sru.k10plus.de/gvk?version=2.0&operation=searchRetrieve&query=pica.ver%3DHalle%20and%20pica.bbg%3DAcv&recordSchema=picaxml&maximumRecords=5), [pica.ver=Halle and pica.bbg=Acvs](http://sru.k10plus.de/gvk?version=2.0&operation=searchRetrieve&query=pica.ver%3DHalle%20and%20pica.bbg%3DAcvs&recordSchema=picaxml&maximumRecords=5), [pica.ver=Halle and pica.bbg=Adr](http://sru.k10plus.de/gvk?version=2.0&operation=searchRetrieve&query=pica.ver%3DHalle%20and%20pica.bbg%3DAdr&recordSchema=picaxml&maximumRecords=5), [pica.ver=Halle and pica.bbg=Adu](http://sru.k10plus.de/gvk?version=2.0&operation=searchRetrieve&query=pica.ver%3DHalle%20and%20pica.bbg%3DAdu&recordSchema=picaxml&maximumRecords=5), [pica.ver=Halle and pica.bbg=Adv](http://sru.k10plus.de/gvk?version=2.0&operation=searchRetrieve&query=pica.ver%3DHalle%20and%20pica.bbg%3DAdv&recordSchema=picaxml&maximumRecords=5), [pica.ver=Halle and pica.bbg=Oav](http://sru.k10plus.de/gvk?version=2.0&operation=searchRetrieve&query=pica.ver%3DHalle%20and%20pica.bbg%3DOav&recordSchema=picaxml&maximumRecords=5), [pica.ver=Halle and pica.bbg=Obv](http://sru.k10plus.de/gvk?version=2.0&operation=searchRetrieve&query=pica.ver%3DHalle%20and%20pica.bbg%3DObv&recordSchema=picaxml&maximumRecords=5), [pica.ver=Halle and pica.bbg=Ocu](http://sru.k10plus.de/gvk?version=2.0&operation=searchRetrieve&query=pica.ver%3DHalle%20and%20pica.bbg%3DOcu&recordSchema=picaxml&maximumRecords=5), [pica.ver=Halle and pica.bbg=Ocv](http://sru.k10plus.de/gvk?version=2.0&operation=searchRetrieve&query=pica.ver%3DHalle%20and%20pica.bbg%3DOcv&recordSchema=picaxml&maximumRecords=5), [pica.ver=Halle and pica.bbg=Odv](http://sru.k10plus.de/gvk?version=2.0&operation=searchRetrieve&query=pica.ver%3DHalle%20and%20pica.bbg%3DOdv&recordSchema=picaxml&maximumRecords=5)
    - nachträglich wurde eine [Filterung](./utils/filter) der Ergebnismengen nach Veröffentlichungszeitpunkt zwischen 1688 und 1815 vorgenommen
- Datenlizenz: [CC0 1.0](https://www.gbv.de/Verbundzentrale/benutzungs-und-entgeltordnung-der-verbundzentrale)

Siehe auch den entsprechenden [Eintrag im Datenbankverzeichnis](https://uri.gbv.de/database/gvk) vom GBV.

### VD 18 – Das Verzeichnis Deutscher Drucke des 18. Jahrhunderts ([DE-627-5](https://sigel.staatsbibliothek-berlin.de/suche/?isil=DE-627-5))

> In diesem "Verzeichnis der im deutschen Sprachraum erschienenen Drucke des 18. Jahrhunderts" werden alle zwischen 1701 und 1800 in deutscher Sprache oder im deutschen Sprachraum erschienenen Drucke kooperativ erfasst und mit einer individuellen VD18-Nummer versehen. Derzeit (Stand: Februar 2018) enthält die von der Verbundzentrale des Gemeinsamen Bibliotheksverbunds betreute VD18-Datenbank rund 178.000 Monographien, 9.500 mehrbändige Werke mit 28.000 Bänden und ca. 3.300 Zeitschriftentitel. ([Quelle](https://gso.gbv.de/DB=1.65/))

_Search and Retrieve via URL_

- Dokumentation: https://verbundwiki.gbv.de/display/VZG/SRU
- Endpunkt: http://sru.gbv.de/vd18
- Format: [picaxml](http://format.gbv.de/pica/xml)
- Abfragen: [pica.pub=Halle and pica.mak=Abv](http://sru.gbv.de/vd18?version=2.0&operation=searchRetrieve&query=pica.pub%3DHalle%20and%20pica.mak=Abv&recordSchema=picaxml&maximumRecords=5), [pica.pub=Halle and pica.mak=Acv](http://sru.gbv.de/vd18?version=2.0&operation=searchRetrieve&query=pica.pub%3DHalle%20and%20pica.mak=Acv&recordSchema=picaxml&maximumRecords=5), [pica.pub=Halle and pica.mak=Adv](http://sru.gbv.de/vd18?version=2.0&operation=searchRetrieve&query=pica.pub%3DHalle%20and%20pica.mak=Adv&recordSchema=picaxml&maximumRecords=5), [pica.pub=Halle and pica.mak=Obv](http://sru.gbv.de/vd18?version=2.0&operation=searchRetrieve&query=pica.pub%3DHalle%20and%20pica.mak=Obv&recordSchema=picaxml&maximumRecords=5), [pica.pub=Halle and pica.mak=Ocv](http://sru.gbv.de/vd18?version=2.0&operation=searchRetrieve&query=pica.pub%3DHalle%20and%20pica.mak=Ocv&recordSchema=picaxml&maximumRecords=5), [pica.pub=Halle and pica.mak=Odv](http://sru.gbv.de/vd18?version=2.0&operation=searchRetrieve&query=pica.pub%3DHalle%20and%20pica.mak=Odv&recordSchema=picaxml&maximumRecords=5)

Siehe auch den entsprechenden [Eintrag im Datenbankverzeichnis](http://uri.gbv.de/database/vd18) vom GBV.

### Gelehrte Journale und Zeitungen der Aufklärung ([DE-627](https://sigel.staatsbibliothek-berlin.de/suche/?isil=DE-627))

> Seit dem Jahr 2011 werden im Rahmen des Projektes "Gelehrte Journale und Zeitungen als Netzwerke des Wissens im Zeitalter der Aufklärung" die bedeutendsten deutschsprachigen Vertreter der fächerübergreifenden polyhistorischen Zeitschriften bibliographisch und inhaltlich erschlossen sowie in digitalisierter Form verfügbar gemacht. Unter der Trägerschaft der Akademie der Wissenschaften zu Göttingen, mit Arbeitsstellen in Göttingen, Leipzig und München und in Kooperation mit der Niedersächsischen Staats- und Universitätsbibliothek Göttingen, der Universitätsbibliothek Leipzig und der Bayerischen Staatsbibliothek München wird mit dieser Datenbank bis zum Jahr 2025 ein Quellenfundus aus 323 Zeitschriften (ca. 2.775 Bände, ca. 1.260.000 Seiten) entstehen. Der Erschließungszeitraum reicht von 1688 bis 1815 und bildet damit das gesamte Spektrum der Gelehrten Journale und Zeitungen der Aufklärung ab. ([Quelle](https://gelehrte-journale.de/ueber-uns/projekte-und-datenbanken/))

_Search and Retrieve via URL_

- Dokumentation: https://verbundwiki.gbv.de/display/VZG/SRU
- Endpunkt: http://sru.gbv.de/gjz18
- Format: [picaxml](http://format.gbv.de/pica/xml)
- Abfragen: [pica.plc=Halle](http://sru.gbv.de/gjz18?version=2.0&operation=searchRetrieve&query=pica.plc%3DHalle&recordSchema=picaxml&maximumRecords=5), [pica.zzd=ZDB-ID](http://sru.gbv.de/gjz18?version=2.0&operation=searchRetrieve&query=pica.zzd%3D9825228&recordSchema=picaxml&maximumRecords=5) (hier: [9825228](https://ld.zdb-services.de/data/9825228))
- Datenlizenz: [CC BY-SA 4.0](https://gelehrte-journale.de/impressum/)

Siehe auch den entsprechenden [Eintrag im Datenbankverzeichnis](http://uri.gbv.de/database/gjz18) vom GBV.

## Verwendete Software

- [Bash](https://www.gnu.org/software/bash/)
    - [jq](https://stedolan.github.io/jq/)
- [Perl](https://www.perl.org/)
    - [Catmandu](https://metacpan.org/release/Catmandu) ([Handbook](https://librecat.org/Catmandu/))
        - [Catmandu::PICA](https://metacpan.org/release/Catmandu-PICA)
        - [Catmandu::SRU](https://metacpan.org/release/Catmandu-SRU)
- [R](https://www.r-project.org/)
    - [jsonlite](https://cran.r-project.org/web/packages/jsonlite/index.html)
